#FROM openjdk:8-jre-alpine
FROM amazoncorretto:8u322-alpine-jre

RUN addgroup -S spring && adduser -S spring -G spring

RUN  apk update
RUN  apk add tzdata

USER spring:spring
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} notification.jar
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=dev", "/notification.jar", "io.dataq.message.notification.NotificationApplication"]
