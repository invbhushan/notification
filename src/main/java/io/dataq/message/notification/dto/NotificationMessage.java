package io.dataq.message.notification.dto;

import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
public class NotificationMessage {

    private String batchId;
    private String batchExecutionStatus;
    private String batchExecutionLink;
    private String notificationMode;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getBatchExecutionStatus() {
        return batchExecutionStatus;
    }

    public void setBatchExecutionStatus(String batchExecutionStatus) {
        this.batchExecutionStatus = batchExecutionStatus;
    }

    public String getBatchExecutionLink() {
        return batchExecutionLink;
    }

    public void setBatchExecutionLink(String batchExecutionLink) {
        this.batchExecutionLink = batchExecutionLink;
    }

    public String getNotificationMode() {
        return notificationMode;
    }

    public void setNotificationMode(String notificationMode) {
        this.notificationMode = notificationMode;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", NotificationMessage.class.getSimpleName() + "[", "]")
                .add("batchId=" + batchId)
                .add("batchExecutionStatus=" + batchExecutionStatus)
                .add("batchExecutionLink=" + batchExecutionLink)
                .add("notificationMode=" + notificationMode).toString();
    }
}
