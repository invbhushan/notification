package io.dataq.message.notification.service;

import io.dataq.message.notification.dto.NotificationMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotificationReceiver {

    @JmsListener(destination = "notificationQueue", containerFactory = "notificationFactory")
    public void receiveMessage(NotificationMessage notificationMessage){
        log.info("Message received :"+notificationMessage);
    }
}
